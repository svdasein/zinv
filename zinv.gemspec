# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'zinv/version'

Gem::Specification.new do |spec|
  spec.name          = "zinv"
  spec.version       = Zinv::VERSION
  spec.authors       = ["Dave Parker"]
  spec.email         = ["david.parker _AT_ nsight.com"]

  spec.summary       = %q{Ansible dynamic inventory script for hosts monitored by zabbix}
  spec.homepage      = "https://gitlab.com/svdasein/zinv"
  spec.license       = "MIT"

  # Prevent pushing this gem to RubyGems.org by setting 'allowed_push_host', or
  # delete this section to allow pushing this gem to any host.
  if spec.respond_to?(:metadata)
    spec.metadata['allowed_push_host'] = "https://rubygems.org"
  else
    raise "RubyGems 2.0 or newer is required to protect against public gem pushes."
  end

  spec.files         = `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler"
  spec.add_development_dependency "rake"
  spec.add_runtime_dependency "zabbix-api-simple"
  spec.add_runtime_dependency "optimist"
  spec.add_runtime_dependency "amazing_print"
end
